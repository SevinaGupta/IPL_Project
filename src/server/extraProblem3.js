const fs = require('fs');
const {matchData, deliveriesData } = require("./ipl.js")

const detailsOfBatsman = function(id,seasonData){

	deliveriesData.reduce((element,currentValue)=>{
		const {match_id,batsman,batsman_runs} = currentValue
		if(match_id === id){
			if(element[batsman])
			{
				element[batsman]["runs"] += parseInt(batsman_runs)
                element[batsman]["balls"] += 1	
			}
			else {
				element[batsman] = {runs: parseInt(batsman_runs),balls: 1}
			}
		}
		return element
		},seasonData)
return seasonData
}


const dataOfMatch = matchData.reduce((element,currentValue)=>{
	const {id,season} = currentValue

	if(element[season])
	{
		let seasonData = element[season]
		element[season] = detailsOfBatsman(id,seasonData)
	} 
	else {
        let seasonData = {}
        let output = detailsOfBatsman(id, {})
        element[season] = output
    }
 return element
},{})


// const getStrikeRateOfPlayer = Object.keys(dataOfMatch).reduce((element,currentValue)=>{

//    const {runs,ballsFaced} = dataOfMatch[currentValue]
//    element[currentValue] = { strikeRate: (runs / ballsFaced * 100) }
//  return element
// },{})

const jsonData = JSON.stringify(dataOfMatch)
fs.writeFile('../public/output/strikeRateOfBatsman.json', jsonData, (err) => {
    if (err) {
        console.log(err)
    } else {
        console.log("success")
    }
})