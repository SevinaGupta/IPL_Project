const fs = require('fs');
const { matchData} = require("./ipl.js")



const winMatchesPerYear = matchData.reduce((result, currentValue) => {
    let { season,winner }  = currentValue
	console.log(result)
	//console.log(currentValue)
    if (result[season]) {
        if( result[season][winner]!=" "){
            result[season][winner] += 1
        }
		else{
            result[season][winner] = 1
        }
    } 
	else {
        result[season] = {}
        result[season][winner] = 1
    }
    return result
}, {})

//module.exports = {winMatchesPerYear};

const jsonData = JSON.stringify(winMatchesPerYear)
fs.writeFile('../public/output/matchesWonPerTeamPerYear.json', jsonData, (err) => {
    if (err) {
        console.log(err)
    } else {
        console.log("success")
    }
})

