const Highcharts = require("highcharts")
const getData = require("./utils.js")

const initializeChart = (data) => {
    console.log(data)
    const years = Object.keys(data)
    const matches = Object.values(data)
    Highcharts.chart('container', {
        title: {
            text: "Match Per Year"
        },
        subtitle: {
            text: 'Source: Kaggle IPL Data'
        },
        yAxis: {
            title: {
                text: 'Number of Matches'
            }
        },

        xAxis: {
            accessibility: {
                rangeDescription: years
                
            }
        },

        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },

        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
                pointStart: 2008
            }
        },

        series: [{
            name: 'Matches',
            data: matches
        }],

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }

    });
}

getData("../public/output/matchesPerYear.json", initializeChart) 