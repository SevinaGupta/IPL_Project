
  
const fs = require('fs');
const { matchData, deliveriesData } = require("./ipl.js")



const ExtraRunsPerTeam = matchData.reduce((result, currentValue) => {
    const  { id }  = currentValue
    if (currentValue.season === "2016") {
		
        deliveriesData.forEach((Items) => {
            if (Items.match_id === id) {
                const { bowlingTeam, extraRuns } = Items
                if (result[bowlingTeam]) {

                    result[bowlingTeam] += parseInt(extraRuns)
                } else {
                    result[bowlingTeam] = parseInt(extraRuns)
                }
            }
        })
    }
	
    return result
}, {})

const jsonData = JSON.stringify(ExtraRunsPerTeam)
fs.writeFile('../public/output/findExtraRunsPerTeamIn2016.json', jsonData, (err) => {
    if (err) {
        console.log(err)
    } else {
        console.log("success")
    }
})