const fs = require('fs');
const { matchData } = require("./ipl.js")


const playerOfMatchAward = matchData.reduce((element, currentValue)=>{
	const {id , season, player_of_match } = currentValue
	//console.log(element[season])
	if(element[season]){
		if(element[season][player_of_match]){
			//console.log(element[season][player_of_match])
			element[season][player_of_match] += 1	
		}
		else{
			element[season][player_of_match] = 1
		}
			//console.log(element[season][player_of_match])
	}
	else{
		element[season] = {}
	}
	//console.log(element)

	return element

},{})


const highestNumberOfPlayerOfAward = Object.values(playerOfMatchAward).reduce((element, currentValue )=>{


	let [season] = Object.keys(playerOfMatchAward).filter(eachItem => playerOfMatchAward[eachItem] === currentValue)
		
	let key = Object.keys(currentValue)
	let maxValue = Math.max(...Object.values(currentValue))


	//console.log(season)

	key.forEach(eachItem=>{
		if(currentValue[eachItem]=== maxValue){
			if(element[season]){
				element[season][eachItem] = maxValue
			}else{
				element[season]={}
				element[season][eachItem] = maxValue
			}
		}
	})
	return element


},{});




const jsonData = JSON.stringify(highestNumberOfPlayerOfAward)
//console.log(jsonData)
fs.writeFile('../public/output/playerOfMatchAward.json', jsonData, (err) => {
    if (err) {
        console.log(err)
    } else {
        console.log("success")
    }
})