const fs = require('fs');
const {matchData, deliveriesData } = require("./ipl.js")

const dismissedPlayerData = deliveriesData.reduce((element, currentValue)=>{
	const {blower , player_dismissed} = currentValue
	if(player_dismissed != " "){
		let dismissedBowlerAndBatsman = blower + " " + player_dismissed
		if(element[dismissedBowlerAndBatsman]){
			element[dismissedBowlerAndBatsman] += 1
		}
		else{
			element[dismissedBowlerAndBatsman] = 1
		}
	}

	return element
},{})

const highestNumberOfPlayerDismissedBySamePlayer = Math.max(...Object.values(dismissedPlayerData))

const jsonData = JSON.stringify(highestNumberOfPlayerDismissedBySamePlayer)
fs.writeFile('../public/output/highestNumberOfPlayerDismissedBySamePlayer.json', jsonData, (err) => {
    if (err) {
        console.log(err)
    } else {
        console.log("success")
    }
})

