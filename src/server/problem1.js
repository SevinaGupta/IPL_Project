const fs = require('fs');
const { matchData } = require("./ipl.js")



const MatchesPerYear = matchData.reduce((result, currentValue) => {
    let season  = currentValue
    if (result[season]) {
        result[season] += 1
    }
	else {
        result[season] = 1
    }
    return result}, {})

const jsonData = JSON.stringify(MatchesPerYear)
fs.writeFile('../public/output/matchesPerYear.json', jsonData, (err) => {
    if (err) {
        console.log(err)
    } else {
        console.log("success")
    }
})


