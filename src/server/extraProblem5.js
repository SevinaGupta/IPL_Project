const fs = require('fs');
const {matchData, deliveriesData } = require("./ipl.js")


const runsInSuperOvers = deliveriesData.reduce((element,currentValue)=>{
	const{is_super_over, bowler,total_runs} = currentValue
	if(is_super_over !== "0")
	{
		if(element[bowler]){
			element[bowler]["runs"] += parseInt(total_runs)
			element[bowler]["balls"] +=1
		}
		else{
			element[bowler] = {runs: parseInt(total_runs),balls:1}
		}
	}
	//console.log(element)
	return element
},{})


const economyInSuperOvers = Object.keys(runsInSuperOvers).reduce((element,currentValue)=>{
const bowlerData = runsInSuperOvers[currentValue]
const {runs , balls} = bowlerData
const economy = (runs/(balls/6)).toFixed(2)
// const over = runsInSuperOvers[currentValue]["balls"]/6
// const economy = runsInSuperOvers[currentValue]["runs"]/over
element[currentValue] = {economy}

return element

},{})


const bestEconomy = Math.min(...Object.values(economyInSuperOvers).map(eachItem => {
    return eachItem["economy"]
}))

const bowlerBestEconomyInSuperOvers = Object.keys(economyInSuperOvers).find(eachValue => (economyInSuperOvers[eachValue]["economy"] == bestEconomy))



const jsonData = JSON.stringify(bowlerBestEconomyInSuperOvers)
fs.writeFile('../public/output/bestEconomyInSuperOver.json', jsonData, (err) => {
    if (err) {
        console.log(err)
    } else {
        console.log("success")
    }
})